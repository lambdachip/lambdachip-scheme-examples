;; AHT20 is a small sized temperature sensor with SMT footprint.
;; datasheet of AHT20 please see
;; https://cdn.sparkfun.com/assets/d/2/b/e/d/AHT20.pdf

(define AHT20_DEFAULT_ADDRESS 56) ;; 0x38

(define (AHT20-enable!)
  (i2c-write-list! 'dev_i2c2 AHT20_DEFAULT_ADDRESS '(172 51 0))) ;; 0xac 0x33 0x00)

(define (parse-temperature! dev)
  (let* ((l (i2c-read-bytevector! dev AHT20_DEFAULT_ADDRESS 7)))
    (display "temperature = ")
    (cond ((bytevector? l) (display (- (* (/ (+ (* (- (bytevector-u8-ref l 3) (* 16 (floor (/ (bytevector-u8-ref l 3) 16)))) 256) (bytevector-u8-ref l 4)) 4096) 200.0) 50)))
          (else (display "None")))
    (newline)

    (display "humidity = ")
    (cond ((bytevector? l) (display (* (/ (+ (* 256 (bytevector-u8-ref l 1)) (bytevector-u8-ref l 2)) 65536) 1.0)))
          (else (display "None")))
    (newline)))


;; -------------------------- floor function -----------------------------------
(define (floor_lp1 n x)
  (if (> n x)
      n
      (floor_lp1 (* 2 n) x)))

(define (floor_lp2 delta lower upper x)
  (if (> 1 delta)
      lower
      (if (> (- upper delta) x)
          (floor_lp2 (/ delta 2) lower (- upper delta) x)
          (floor_lp2 (/ delta 2) (+ lower delta) upper x))))

(define (floor x)
  (let ((lim (floor_lp1 1 x)))
    (floor_lp2 (/ lim 4) (/ lim 2) lim x)))
;; -------------------------- floor function -----------------------------------

(define (loop x)
  (AHT20-enable!)
  (usleep 250000) ;; sleep 0.25 second
  (parse-temperature! 'dev_i2c2)
  (display "loop x = ")
  (display x)
  (newline)
  (usleep 250000) ;; sleep 0.25 second
  (if (= 1 x)
      #t
      (loop (- x 1))))

(loop 1000)
