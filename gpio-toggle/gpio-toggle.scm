(define (blink n)
  (usleep 333333)
  (newline)
  (display "loop n = ")
  (display n)
  (gpio-toggle! 'dev_led2)
  (if (<= n 0)
      0
      (blink (- n 1))))

(ble-reset!)
(ble-enable!)
(usleep 100000)

(gpio-set! 'dev_led0 #f)
(gpio-set! 'dev_led1 #f)
(gpio-set! 'dev_led2 #f)
(gpio-set! 'dev_led3 #f)

(blink 100)
(display "end\n")

