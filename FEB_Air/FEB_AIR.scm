;; AHT20 is a small sized temperature sensor with SMT footprint.
;; datasheet of AHT20 please see
;; https://cdn.sparkfun.com/assets/d/2/b/e/d/AHT20.pdf

(define AHT20_DEFAULT_ADDRESS 56) ;; 0x38

(define (AHT20-enable! dev)
  (i2c-write-list! dev AHT20_DEFAULT_ADDRESS '(172 51 0))) ;; 0xac 0x33 0x00)

(define (parse-temperature! dev)
  (let* ((l (i2c-read-bytevector! dev AHT20_DEFAULT_ADDRESS 7))
         ;; (b0 (bytevector-u8-ref l 0))
         (b1 (bytevector-u8-ref l 1))
         (b2 (bytevector-u8-ref l 2))
         (b3 (bytevector-u8-ref l 3))
         (b4 (bytevector-u8-ref l 4))
         ;; (b5 (bytevector-u8-ref l 5))
         ;; (b6 (bytevector-u8-ref l 6))
         )
    (display "temperature = ")
    (cond ((bytevector? l) (display (- (* (/ (+ (* (- b3 (* 16 (floor (/ b3 16)))) 256) b4) 4096) 200.0) 50)))
          (else (display "None")))
    (newline)

    (display "humidity = ")
    (cond ((bytevector? l) (display (* (/ (+ (* 256 b1) b2) 65536) 1.0)))
          (else (display "None")))
    (newline)))

;; -------------------- AHT 20 end --------------------------
;; -------------------- CCS811 start ------------------------
(define CCS811_ADDR 90) ;; ADDR pin low
;; (define CCS811_ADDR 91) ;; ADDR pin high

(define CCS811_REG_SW_RESET 255) ;; 0xFF
(define CCS811_REG_HW_ID 32) ;; 0x20
(define CCS811_HW_ID 129) ;; 0x81
(define CCS811_BOOTLOADER_APP_START 244) ;; 0xF4
(define CCS811_REG_MEAS_MODE 1) ;; 0x01
(define CCS811_REG_ENV_DATA 5) ;;0x05
(define CCS811_REG_ALG_RESULT_DATA 2) ;; 0x02


(define CCS811_MODE_0 0) ;; Idle (Measurements are disabled in this mode)
(define CCS811_MODE_1 1) ;; Constant power mode, IAQ measurement every second
(define CCS811_MODE_2 2) ;; Pulse heating mode IAQ measurement every 10 seconds
(define CCS811_MODE_3 3) ;; Low power pulse heating mode IAQ measurement every 60 seconds
(define CCS811_MODE_4 4) ;; Constant power mode, sensor measurement every 250ms


(define (ccs811_software_reset dev)
  (i2c-write-bytevector! dev CCS811_ADDR #u8(255 17 229 114 138)) ;; CCS811_REG_SW_RESET (0xFF)  0x11 0xE5 0x72 0x8A
  (usleep 10000) ;; sleep 10ms
  (i2c-read-bytevector! dev CCS811_ADDR 100)
  (display "read 100 bytes\n")
  (i2c-write-bytevector! dev CCS811_ADDR #u8(32)) ;; CCS811_REG_HW_ID
  (if (= CCS811_HW_ID (bytevector-u8-ref
                       (i2c-read-bytevector! dev CCS811_ADDR 1)
                       0)) ;; read addr
      (display "REG read OK\n")
      (display "REG read NG\n"))
  (usleep 20000))

(define (ccs811_app_start dev)
  ;; (display "before APP_START\n")
  (i2c-write-bytevector! dev CCS811_ADDR #u8(244)) ;; CCS811_BOOTLOADER_APP_START 0xF4
  (display (i2c-read-bytevector! dev CCS811_ADDR 10))
  ;; (display "after APP_START\n")
  )

(define (ccs811_set_measure_mode dev measurement)
  (display "before ccs811_set_measure_mode\n")
  (i2c-write-list! dev CCS811_ADDR (list CCS811_REG_MEAS_MODE measurement)) ;; CCS811_REG_MEAS_MODE
  (display (i2c-read-bytevector! dev CCS811_ADDR 10))
  (pk "after ccs811_set_measure_mode, measurement = " measurement)
  (newline))

(define (ccs811_get_measure_mode dev)
  (display "ccs811_get_measure_mode ")
  (i2c-write-list! dev CCS811_ADDR (list CCS811_REG_MEAS_MODE))
  (display (i2c-read-bytevector! dev CCS811_ADDR 10))
  (newline))

;; (define (ccs811_set_envdata temperature humidity)
;;   (display "before (ccs811_set_envdata 25 50)\n")
;; ;;   ;; (i2c-write-list! 'dev_i2c3 CCS811_ADDR (list CCS811_REG_ENV_DATA (* 2 humidity) 0 (* 2 (+ temperature 25)) 0)) ;; NG: CMAKE_SOURCE_DIR/lambdachip/gc.c:1103 gc_pool_malloc: BUG: closures are not allocated from pool!
;;   (i2c-write-bytevector! 'dev_i2c3 CCS811_ADDR #u8(5 100 0 100 0))
;;   (i2c-read-bytevector! 'dev_i2c3 CCS811_ADDR 10)
;;   (display "after (ccs811_set_envdata 25 50)\n")
;;   )

(define (ccs811_measure_write dev)
  (i2c-write-bytevector! dev CCS811_ADDR #u8(2)) ;; CCS811_REG_ALG_RESULT_DATA
  (usleep 100000))

(define (ccs811_measure dev)
  ;; (display (i2c-read-bytevector! dev CCS811_ADDR 8))
  ;; ;; (i2c-write-bytevector! dev CCS811_ADDR #u8(2)) ;; CCS811_REG_ALG_RESULT_DATA
  ;; ;; (usleep 10000) ;; NG, cannot write expr before let
  (let* ((l (i2c-read-bytevector! dev CCS811_ADDR 8))
         (b0 (bytevector-u8-ref l 0))
         (b1 (bytevector-u8-ref l 1))
         (b2 (bytevector-u8-ref l 2))
         (b3 (bytevector-u8-ref l 3))
         (status (bytevector-u8-ref l 4))
         (error_id (bytevector-u8-ref l 5))
         (b6 (bytevector-u8-ref l 6))
         (b7 (bytevector-u8-ref l 7)))
    (display l)
    (newline)
    (display "eCO2 = ")
    (display (+ (* 256 b0) b1))
    (display ", ")
    (display "eTVOC = ")
    (display (+ (* 256 b2) b3))
    (display ", ")
    (display "STATUS = ")
    (display status)
    (display ", ")
    (display "error_id = ")
    (display error_id)
    (newline)
    (cond ((= error_id 2) (display "The CCS811 received an I²C read request to a mailbox ID that is invalid\n"))
          ((= error_id 0) #true)
          (else (display "Other error\n")))))



(define thresh 0)
(define interrupt 0)
(define measurement (+ (* 4 thresh) (* 8 interrupt) (* 16 CCS811_MODE_4)))

(define (init)
  (display "AT+NAME?\n")
  (ble-reset!)
  (usleep 100000) ;; 0.1s
  (display "AT+MODE?\n")
  (usleep 100000) ;; 0.1s
  (display "AT+AUTO+++=Y\n")
  (usleep 100000) ;; 0.1s
  (display "AT+POWER?\n")
  (newline)
  (usleep 3000000) ;; 3s

  (device-configure! 'dev_gpio_pb6)
  (device-configure! 'dev_gpio_pb7)

  (gpio-set! 'dev_gpio_pb6 #f) ;; ___INT
  (gpio-set! 'dev_gpio_pb7 #t) ;; set ____WAKE to low

  (usleep 100000)

  (ccs811_software_reset 'dev_i2c3)
  (ccs811_app_start 'dev_i2c3)

  (ccs811_set_measure_mode 'dev_i2c3 measurement)

  (ccs811_get_measure_mode 'dev_i2c3)
  ;; (ccs811_set_envdata 25 50) ;; set temperature to 25 degree and humidity to 50%
  )



(define (loop)
  (gpio-toggle! 'dev_led3)
  (display "(ccs811_measure) ")
  (ccs811_measure_write 'dev_i2c3)
  (usleep 1000000) ;; 1s
  (ccs811_measure 'dev_i2c3)
  (newline)
  (usleep 100000) ;; 0.1s
  (AHT20-enable! 'dev_i2c3)
  (usleep 500000) ;; 0.5s
  (parse-temperature! 'dev_i2c3)
  (usleep 3400000) ;; 3.4s
  (loop))


(init)
(loop)
;; -------------------------------------
;; -------------------------------------
