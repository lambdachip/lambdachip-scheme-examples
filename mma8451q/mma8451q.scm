;; minimum requirement, laco v0.3.0, global variable required
;; global define for MMA8451 registers
(define MMA8451_DEFAULT_ADDRESS 29) ;; 0x1D
(define MMA8451_REG_WHOAMI 13) ;; 0x0D
(define MMA8451_REG_CTRL_REG1 42) ;; 0x2A


(define (convert-to-int16 a b)
  (let ((c (+ (* 256 a) b)))
    (if (> c 32767)
        (- c 65536)
        c)))


(define (mma8451_connection_check!)
  (let* ((ret (i2c-read-byte! 'dev_i2c2 MMA8451_DEFAULT_ADDRESS MMA8451_REG_WHOAMI)))
    (if ret
        (if (= ret 26)
            #t
            #f)
        #f)))


(define (mma8451_set_state_active!)
  (i2c-write-byte! 'dev_i2c2 MMA8451_DEFAULT_ADDRESS MMA8451_REG_CTRL_REG1 1)
  (let  ((ret (i2c-read-byte! 'dev_i2c2 MMA8451_DEFAULT_ADDRESS MMA8451_REG_CTRL_REG1)))
    (display "ret = ")
    (display ret)
    (newline)
    (if ret
        (if (= ret 1)
            #t
            #f)
        #f)))


(define (mma8451_read_all!)
  (let* ((reg1 (i2c-read-byte! 'dev_i2c2 MMA8451_DEFAULT_ADDRESS 1))
         (reg2 (i2c-read-byte! 'dev_i2c2 MMA8451_DEFAULT_ADDRESS 2))
         (reg3 (i2c-read-byte! 'dev_i2c2 MMA8451_DEFAULT_ADDRESS 3))
         (reg4 (i2c-read-byte! 'dev_i2c2 MMA8451_DEFAULT_ADDRESS 4))
         (reg5 (i2c-read-byte! 'dev_i2c2 MMA8451_DEFAULT_ADDRESS 5))
         (reg6 (i2c-read-byte! 'dev_i2c2 MMA8451_DEFAULT_ADDRESS 6)))
    (display "x = ")
    (display (convert-to-int16 reg1 reg2))
    (display ", y = ")
    (display (convert-to-int16 reg3 reg4))
    (display ", z = ")
    (display (convert-to-int16 reg5 reg6))
    (display "\n")))


(define (loop x)
  (usleep 333333)
  (mma8451_read_all!)
  (if (<= x 0)
      0
      (loop (- x 1))))


(define (main)
  (ble-enable!)
  (if (mma8451_connection_check!)
      (begin (display "MMA8451Q connection OK!\n")
             (mma8451_set_state_active!)
             (loop 90))
      (display "Connection FAIL!\n"))
  (newline))


(main)
