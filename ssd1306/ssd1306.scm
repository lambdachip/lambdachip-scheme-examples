;; ssd1306

;;---------------------------------  oled ---------------------------------------
;; ;; Table 9-1: Command Table
;; ;; 1. Fundamental Command Table
;; set_contrast_control
;; 10.1.7 Set Contrast Control for BANK0 (81h)
(define OLED_CONTRAST_CONTROL 129) ;;0x81

;; Entire Display ON
(define OLED_ENTIRE_DISPLAY_ON_RAM 164) ;; 0xA4
(define OLED_ENTIRE_DISPLAY_ON_IGNORE_RAM 165) ;; 0xA5

;; set normal or inverse display
(define OLED_DISPLAY_NORMAL 166) ;; 0xA6
(define OLED_DISPLAY_INVERSE 167) ;; 0xA7

;; set display on or off
(define OLED_SET_DISPLAY_OFF 174) ;; 0xAE
(define OLED_SET_DISPLAY_ON 175) ;; 0xAF

;; ;; 2. Scrolling Command Table
;; continuous horizontal scroll setup
;; continuous vertical and horizontal scroll setup
;; Deactivate scroll
(define OLED_DEACTIVATE_SCROLL 46) ;; 0x2E

;; activate scroll
(define OLED_ACTIVATE_SCROLL 47) ;; 0x2F


;; set vertical scroll area

;; ;; 3. Addressing Setting Command Table

;; set lower column start address for page addressing mode
(define OLED_LOWER_COLUMN_START_ADDRESS 0) ;; 0x00, ;; 0x00~0x0f

;; set higher column start address for page addressing mode
(define OLED_HIGHER_COLUMN_START_ADDRESS 16) ;; 0x10, ;; 0x10~0x1f


;; set memory addressing mode
(define OLED_MEMORY_ADDRESSING_MODE 32) ;; 0x20
(define OLED_MEMORY_ADDRESSING_HORIZONTAL_MODE 0) ;; 0x20,0x00
(define OLED_MEMORY_ADDRESSING_VERTICAL_MODE 1) ;; 0x20,0x01
(define OLED_MEMORY_ADDRESSING_PAGE_MODE 2) ;; 0x20,0x02


;; set column address
;; Set Page Address

;; Set Page Start Address for Page Addressing Mode
;; Set GDDRAM Page Start Address (PAGE0~PAGE7) for Page Addressing Mode using X[2:0].
(define OLED_PAGE_START_ADDR 176) ;; 0xB0, ;; 0xb0 ~ 0xb7

;; ;; 4. Hardware Configuration (Panel resolution & layout related) Command Table

;; Set Display Start Line
(define OLED_DISPLAY_START_LINE 64) ;; 0x40, ;; 0x40~0x7f, for start from line 0 to 63

;; Set Segment Re-map
;; Set Multiplex Ratio
(define OLED_MULTIPLEX_RATIO 168) ;; 0xA8, ;; Set Multiplex Ratio
;; 0xA8,(0x00~0x1f)

;; Set COM Output Scan Direction
(define OLED_COM_OUTPUT_SCAN_DIRECTION_INCREASE 192) ;; 0xC0, ;; COM0~COM[N-1]
(define OLED_COM_OUTPUT_SCAN_DIRECTION_DECREASE 200) ;; 0xC8, ;; COM[N-1]~COM0

;; Set Display Offset
;; Set vertical shift by COM from 0d~63d The value is reset to 00h after RESET.
(define OLED_DISPLAY_OFFSET 211) ;; 0xD3
;; 0xD3,(0x00~0x1f)

;; Set COM Pins Hardware Configuration
(define OLED_COM_PINS_HARDWARE_CONFIGURATION 218) ;; 0xDA
;; 0xDA,[0x00~0x3F];;

;; ;; 5. Timing & Driving Scheme Setting Command Table
;; Set Display Clock Divide Ratio/Oscillator Frequency
(define OLED_DISPLAY_CLOCK_DIVIDE_RATIO 213) ;; 0xD5

;; Set Pre-charge Period
(define OLED_PRE_CHARGE_PERIOD 217) ;; 0xD9
;; 0xD9,(0x00~0xFF)

;; Set VCOMH Deselect Level
(define OLED_VCOMH_DESELECT_LEVEL 219) ;; 0xDB
;; NOP

;; **********************************
;; Set Segment Re-map (A0h/A1h)
;; A0h, X[0]=0b: column address 0 is mapped to SEG0 (RESET) A1h, X[0]=1b: column address 127 is mapped to SEG0
(define OLED_SEGMENT_REMAP_NORMAL 160) ;; 0xA0
(define OLED_SEGMENT_REMAP_REVERSE 161) ;; 0xA1

;; 10.1.8 Set Segment Re-map (A0h/A1h)
;; } commmand_t;;

;; ;; Table 9-2 : Read Command Table
;; Status Register Read


;; #define IS_ADDRESSING_MODE(MODE) (((MODE) == MEMORY_ADDRESSING_HORIZONTAL_MODE) || \
;;                                   ((MODE) == MEMORY_ADDRESSING_VERTICAL_MODE) || \
;;                                   ((MODE) == MEMORY_ADDRESSING_PAGE_MODE))

(define OLED_PAGE_COUNT 8)
(define OLED_BYTES_IN_PAGE 128)
;; (define OLED_BYTES_IN_PAGE 16)

;;---------------------------------  oled ---------------------------------------
(define (pk str obj)
  (display ";-;-;- ")
  (display str)
  (display " ")
  (display obj)
  (newline)
  obj)


(define (oled->addr oled)
  ;; (car oled)
  (list-ref oled 0)
  )

(define (oled->addressing_mode oled)
  ;; (cdr oled)
  (list-ref oled 1)
  )

(define (oled_write_data oled data)
  (let ((addr (list-ref oled 0)))
    ;; (pk "oled_write_data addr" addr)
    (i2c-write-list! 'dev_i2c2 addr (list 64 data))  ;; 0x40
    )
  )

(define (oled_write_command_1 oled data)
  ;; (usleep 100)
  (let ((addr (list-ref oled 0)))
    (pk "wc1 " data)
    (i2c-write-list! 'dev_i2c2 addr (list 0 data))
    0
    ))

(define (oled_write_command_2 oled data0 data1)
  ;; (usleep 100)
  (let ((addr (list-ref oled 0)))
    (pk "wc2 " data1)
    (i2c-write-list! 'dev_i2c2 addr (list 0 data0 data1))
    0
    )
  )

(define (oled_init oled i2c_addr)
  (usleep 200000)
  ;; display off
  ;; (oled_write_command_1 oled OLED_SET_DISPLAY_OFF) ;; 174 ;; 0xAE
  ;; Set Multiplex Ratio ;; 168 63, 0xA8 0x3F
  (oled_write_command_2 oled OLED_MULTIPLEX_RATIO 63)

  ;; 211 0, 0xD3 0x00
  (oled_write_command_2 oled OLED_DISPLAY_OFFSET 0)

  ;; set display start line at line 0 ;; 64, 0x40
  (oled_write_command_1 oled OLED_DISPLAY_START_LINE)

  ;; segment redirection setting ;; 161, 0xA1
  (oled_write_command_1 oled OLED_SEGMENT_REMAP_REVERSE)

  ;; Set COM Output Scan Direction ;; 200 0xC8
  (oled_write_command_1 oled OLED_COM_OUTPUT_SCAN_DIRECTION_DECREASE)

  ;; only for 128x64, Set COM Pins hardware configuration ;; 218 18, 0xDA 0x12
  ;; (oled_write_command_2 oled OLED_COM_PINS_HARDWARE_CONFIGURATION 18)
  (oled_write_command_2 oled OLED_COM_PINS_HARDWARE_CONFIGURATION 2)

  ;; Set Contrast Control, 0x81 0x7F, 129 127
  (oled_write_command_2 oled OLED_CONTRAST_CONTROL 1)

  ;; 0xA4
  (oled_write_command_1 oled 164)

  ;; 0xA6
  (oled_write_command_1 oled 166)

  ;; 0xD5 0x80
  (oled_write_command_2 oled 213 128)
  ;; ;; (oled_write_command_1 (oled)

  ;; ;; Set Lower Column Start Address for Page Addressing Mode
  ;; (oled_write_command_1 oled OLED_LOWER_COLUMN_START_ADDRESS)

  ;; ;; Set Higher Column Start Address for Page Addressing Mode
  ;; (oled_write_command_1 oled OLED_HIGHER_COLUMN_START_ADDRESS)

  ;; ;; Set Memory Addressing Mode
  ;; ;; OLED_MEMORY_ADDRESSING_HORIZONTAL_MODE, OLED_MEMORY_ADDRESSING_VERTICAL_MODE or OLED_MEMORY_ADDRESSING_PAGE_MODE
  ;; ;; uint8_t mode = OLED_MEMORY_ADDRESSING_HORIZONTAL_MODE
  ;; ;; oled->addressing_mode = mode
  ;; (oled_write_command_2 oled OLED_MEMORY_ADDRESSING_MODE OLED_MEMORY_ADDRESSING_HORIZONTAL_MODE) ;; ;;(oled->addressing_mode oled))

  ;; ;;---set high column address
  ;; (oled_write_command_1 oled (+ OLED_PAGE_START_ADDR  0))

  ;; ;; normal or inverse
  ;; (oled_write_command_1 oled OLED_DISPLAY_NORMAL)

  ;; (oled_write_command_2 oled OLED_DISPLAY_CLOCK_DIVIDE_RATIO 240) ;; 0xf0

  ;; (oled_write_command_2 oled OLED_PRE_CHARGE_PERIOD 34) ;; 0x22

  ;; (oled_write_command_2 oled OLED_VCOMH_DESELECT_LEVEL 73) ;; 0x49

  ;; ;; ;; ****************************************
  )

(define (oled_display_on oled)
  ;; ;; SET DCDC命令
  ;; (oled_write_command_1 oled 141) ;; 0x8D
  ;; ;; DCDC ON
  ;; (oled_write_command_1 oled 20) ;; 0x14
  (oled_write_command_2 oled 141 20) ;; 0x8D 0x14
  ;; DISPLAY ON
  (oled_write_command_1 oled OLED_SET_DISPLAY_ON) ;; 0xAF
  0
  )

(define (clear_and_draw_pattern oled)
  (display "(clear_and_draw_pattern oled)")
  (newline)
  (oled_display_on oled)
  (usleep 50000)
  ;; (usleep 50000)
  (oled_draw_pattern oled)
  (usleep 500000)
  ;; (oled_clear oled)
  )

(define (oled_clear_lp1 oled x lim)
  (oled_write_data oled 0)
  (if (= x (- lim 1))
      (display "oled_clear_lp1 done\n")
      ;; x
      (oled_clear_lp1 oled (+ x 1) lim)))

(define (oled_clear_lp0 oled x lim)
  ;; (pk "(oled_clear_lp0 oled x lim), x = " x)
  (oled_write_command_1 oled (+ x OLED_PAGE_START_ADDR))
  (oled_write_command_1 oled OLED_LOWER_COLUMN_START_ADDRESS)
  (oled_write_command_1 oled OLED_HIGHER_COLUMN_START_ADDRESS)

  (oled_clear_lp1 oled 0 OLED_BYTES_IN_PAGE)

  (if (= x (- lim 1))
      ;; x
      (display "oled_clear_lp0 done\n")
      (oled_clear_lp0 oled (+ x 1) lim))
  )

(define (oled_clear oled)
  ;; (display "(oled_clear oled)\n")
  (oled_clear_lp0 oled 0 OLED_PAGE_COUNT)
  )

(define (oled_draw_pattern_lp1 oled x lim)
  ;; (oled_write_data oled 255)
  ;; (oled_write_data oled 165)
  ;; (oled_write_data oled 90)
  (oled_write_data oled x)
  ;; (oled_write_data oled (+ x 128))
  (if (= x (- lim 1))
      x
      (oled_draw_pattern_lp1 oled (+ x 1) lim))
  )

(define (oled_draw_pattern_lp0 oled x lim)
  ;; (pk "(oled_draw_pattern_lp0 oled x lim), x = " x)
  (oled_write_command_1 oled (+ x OLED_PAGE_START_ADDR))
  (oled_write_command_1 oled OLED_LOWER_COLUMN_START_ADDRESS)
  (oled_write_command_1 oled OLED_HIGHER_COLUMN_START_ADDRESS)

  ;; (oled_draw_pattern_lp1 oled 0 OLED_BYTES_IN_PAGE)
  (oled_draw_pattern_lp1 oled 0 128)
  ;; (oled_draw_pattern_lp1 oled 32 64)
  ;; (oled_draw_pattern_lp1 oled 64 96)
  ;; (oled_draw_pattern_lp1 oled 96 OLED_BYTES_IN_PAGE)
  (if (= x (- lim 1))
      x
      (oled_draw_pattern_lp0 oled (+ x 1) lim))
  )

(define (oled_draw_pattern oled)
  (oled_draw_pattern_lp0 oled 0 OLED_PAGE_COUNT)
  )

;; 60 is captured from pulseview
(define oled (list 60 OLED_MEMORY_ADDRESSING_HORIZONTAL_MODE))
;; (define oled '(120 OLED_MEMORY_ADDRESSING_HORIZONTAL_MODE))

(oled_init oled)
(clear_and_draw_pattern oled)
